<?php

return [
    'config_dir'  => 'novum.cjib',
    'namespace'   => 'NovumCjib',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'cjib.demo.novum.nu',
    'test_domain' => 'cjib.tst.demo.novum.nu',
    'dev_domain'  => 'cjib.demo.novum.nuidev.nl',

];
