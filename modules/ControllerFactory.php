<?php
namespace NovumCjib;

use _Default\ControllerFactory as _DefaultControllerFactory;
use _DefaultApi\Home\Controller;
use Core\IControllerFactory;
use Core\MainController;

class ControllerFactory extends _DefaultControllerFactory implements IControllerFactory
{
    public function getController(array $aGet, array $aPost, string $sNamespace = null):MainController
    {
        $sRequestUri = $_SERVER['REQUEST_URI'];


        $sRequestUri = preg_replace('/^\/v1/', '', $sRequestUri);
        $aParts = explode('?', $sRequestUri);
        $sRequestUriNoVars = $aParts[0];

        if(in_array($sRequestUri, ['/', '/rest' , '/doc/datatypes', '/endpoints']))
        {
            $oController = new Controller($aGet, $aPost);
        }

        return $oController;
    }
}
